
#include <nanobind/nanobind.h>

#include "common/common_bindings.h"
#include "pure/definition.h"

namespace nb = nanobind;

NB_MODULE(pure, module) {
  physics::bindings::pure::wrap_model_definition(module);
  physics::bindings::common_bindings(module);
}
