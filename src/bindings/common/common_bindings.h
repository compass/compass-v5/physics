#include <icus/field-binding.h>
#include <nanobind_utils/as-bytes.h>
#include <nanobind_utils/enum.h>
#include <nanobind_utils/numpy-dtype.h>
#include <physics/model.h>
#include <physics/sanity_check.h>
#include <physics/utilities.h>

namespace physics {
namespace bindings {

namespace nbu = compass::nanobind;

void common_bindings(nb::module_ module) {

  module.attr("nb_natural_variables") = nb_natural_variables;
  module.attr("nb_dof") = nb_dof;

  module.def("init_contexts_info", &init_contexts_info);

  // bind State and its Field
  nb::class_<State> pyState{module, "State"};
  pyState.def(nb::init<>());
  pyState.def_prop_ro(
      "pressure",
      [](nb::handle h) {
        return nb::ndarray<Real, nb::numpy>(&(nb::cast<State &>(h).pressure),
                                            {nb_phases}, h);
      },
      nb::keep_alive<0, 1>());
  pyState.def_rw("temperature", &State::temperature);
  pyState.def_prop_ro(
      "molar_fractions",
      [](nb::handle h) {
        return nb::ndarray<Real, nb::numpy>(
            &(nb::cast<State &>(h).molar_fractions), {nb_phases, nb_components},
            h);
      },
      nb::keep_alive<0, 1>());
  pyState.def_prop_ro(
      "saturation",
      [](nb::handle h) {
        return nb::ndarray<Real, nb::numpy>(&(nb::cast<State &>(h).saturation),
                                            {nb_phases}, h);
      },
      nb::keep_alive<0, 1>());

  nbu::add_numpy_dtype(
      pyState, {{"pressure", "double", {nb_phases}},
                {"temperature", "double"},
                {"molar_fractions", "double", {nb_phases, nb_components}},
                {"saturation", "double", {nb_phases}}});
  nbu::add_as_bytes(pyState);
  icus::bind_field(module, pyState, "FieldState");

  // bind Xalpha and its Field
  nb::class_<Xalpha> pyXalpha{module, "Xalpha"};
  pyXalpha.def(nb::init<>());
  pyXalpha.def_rw("pressure", &Xalpha::pressure);
  pyXalpha.def_rw("temperature", &Xalpha::temperature);
  pyXalpha.def_prop_ro(
      "molar_fractions",
      [](nb::handle h) {
        return nb::ndarray<Real, nb::numpy>(
            &(nb::cast<Xalpha &>(h).molar_fractions), {nb_components}, h);
      },
      nb::keep_alive<0, 1>());

  nbu::add_numpy_dtype(pyXalpha,
                       {{"pressure", "double"},
                        {"temperature", "double"},
                        {"molar_fractions", "double", {nb_components}}});
  nbu::add_as_bytes(pyXalpha);
  icus::bind_field(module, pyXalpha, "FieldXalpha");

  // Rocktype_field is already binded with FieldInt

  // bind Accumulation and its Field
  nb::class_<Accumulation> pyAcc{module, "Accumulation"};
  pyAcc.def(nb::init<>());
  pyAcc.def_prop_ro(
      "molar",
      [](nb::handle h) {
        return nb::ndarray<Real, nb::numpy>(
            &(nb::cast<Accumulation &>(h).molar), {nb_components}, h);
      },
      nb::keep_alive<0, 1>());
  pyAcc.def_rw("energy", &Accumulation::energy);

  nbu::add_numpy_dtype(
      pyAcc, {{"molar", "double", {nb_components}}, {"energy", "double"}});
  nbu::add_as_bytes(pyAcc);
  icus::bind_field(module, pyAcc, "FieldAccumulation");

  module.def("test_init_state", &test_init_state);
}
} // namespace bindings
} // namespace physics
