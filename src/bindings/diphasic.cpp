
#include <nanobind/nanobind.h>

#include "common/common_bindings.h"
#include "diphasic/definition.h"

namespace nb = nanobind;

NB_MODULE(diphasic, module) {
  physics::bindings::diphasic::wrap_model_definition(module);
  physics::bindings::common_bindings(module);
}
