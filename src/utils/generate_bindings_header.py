import sys
from pathlib import Path
from functools import partial
from extract_data import extract_data

assert len(sys.argv) == 3
filepath = Path(sys.argv[1])
assert filepath.exists()
physics_name = sys.argv[2]

phases, components, contexts = extract_data(filepath)

dump = partial(print, end="")


def dump_enum_binding(enum_classname, entries):
    enum_name = enum_classname.lower()
    dump(
        f"""
  auto {enum_name} = nbu::bind_enum<{enum_classname}>(module, "{enum_classname}", {{
"""
    )
    dump(
        ", ".join([f"""{{ {enum_classname}::{name}, "{name}"}}""" for name in entries])
    )
    dump(
        f"""
    }});
    icus::bind_field(module, {enum_name}, "{enum_classname}Field", icus::pack<std::underlying_type_t<{enum_classname}>>());
"""
    )


dump(
    f"""#pragma once

/************************************************************

This file is generated from definitions at compile time and
                    is NOT to be edited.

*************************************************************/

#include <icus/field-binding.h>
#include <nanobind/nanobind.h>
#include <nanobind/operators.h>
#include <icus/field-binding.h>
#include <nanobind_utils/enum.h>
#include <physics/{physics_name}.h>

namespace nb = nanobind;
namespace nbu = compass::nanobind;

NB_MAKE_OPAQUE(physics::Context);
NB_MAKE_OPAQUE(physics::Component);
NB_MAKE_OPAQUE(physics::Phase);

namespace physics {{
namespace bindings {{
namespace {physics_name} {{

"""
)

dump(f"void wrap_model_definition(nb::module_& module)")

dump(
    """{

  using namespace physics;

  module.attr("nb_components") = nb_components;
  module.attr("nb_phases") = nb_phases;
  module.attr("nb_contexts") = nb_contexts;

"""
)

dump_enum_binding("Component", components)
dump_enum_binding("Phase", phases)
dump_enum_binding("Context", contexts.keys())

dump(
    f"""
}}

}}  // namespace {physics_name}
}}  // namespace bindings
}}  // namespace physics
"""
)
