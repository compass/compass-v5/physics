if(NOT COMPASS_COMPILED_PHYSICS)
  message(
    FATAL_ERROR
      "

  You must defined at least one physics to be compiled!

")
endif()

macro(generate_bindings_header name)

  set(PHYSICS_DEFINITION_FILE
      ${CMAKE_CURRENT_SOURCE_DIR}/physics/${name}/model.yml)
  if(NOT EXISTS ${PHYSICS_DEFINITION_FILE})
    message(
      FATAL_ERROR
        "You should provide a definition for physics ${name} in ${PHYSICS_DEFINITION_FILE}"
    )
  endif()
  file(MAKE_DIRECTORY bindings/${name})
  set(bindings_header_file
      ${CMAKE_CURRENT_SOURCE_DIR}/bindings/${name}/definition.h)
  set(script ${CMAKE_CURRENT_SOURCE_DIR}/utils/generate_bindings_header.py)
  message(
    STATUS
      "${name} bindings header will be generated to ${bindings_header_file}")
  add_custom_command(
    OUTPUT ${bindings_header_file}
    COMMAND ${Python_EXECUTABLE} ${script} ${PHYSICS_DEFINITION_FILE} ${name} >
            ${bindings_header_file}
    DEPENDS ${script} ${PHYSICS_DEFINITION_FILE}
    VERBATIM)
  add_custom_target(${name}-bindings-header ALL DEPENDS ${bindings_header_file})

endmacro()
