import yaml


def extract_data(filepath):

    with filepath.open() as stream:
        data = yaml.safe_load(stream)

    phases = data["phases"]
    components = data["components"]
    contexts = data["contexts"]

    for enums in [phases, components, contexts.keys()]:
        for s in enums:
            if not s.isidentifier():
                raise RuntimeError(f'"{s}" is not a valid identifier')

    for context_data in contexts.values():
        for phase in context_data["present_phases"]:
            assert phase in phases
            pass

    return phases, components, contexts
