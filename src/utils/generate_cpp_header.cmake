if(NOT COMPASS_COMPILED_PHYSICS)
  message(
    FATAL_ERROR
      "

  You must defined at least one physics to be compiled!

")
endif()

macro(generate_cpp_header name)

  set(PHYSICS_SRC_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/physics/${name})
  set(PHYSICS_DEFINITION_FILE ${PHYSICS_SRC_INCLUDE_DIR}/model.yml)
  if(NOT EXISTS ${PHYSICS_DEFINITION_FILE})
    message(
      FATAL_ERROR
        "You should provide a definition for physics ${name} in ${PHYSICS_DEFINITION_FILE}"
    )
  endif()
  set(cxx_header_file ${PHYSICS_SRC_INCLUDE_DIR}/model.h)
  set(script ${CMAKE_CURRENT_SOURCE_DIR}/utils/generate_cpp_header.py)
  message(
    STATUS "${name} model definition will be generated to ${cxx_header_file}")
  add_custom_command(
    OUTPUT ${cxx_header_file}
    COMMAND ${Python_EXECUTABLE} ${script} ${PHYSICS_DEFINITION_FILE} >
            ${cxx_header_file}
    DEPENDS ${script} ${PHYSICS_DEFINITION_FILE}
    VERBATIM)
  add_custom_target(${name}-cxx-header ALL DEPENDS ${cxx_header_file})

endmacro()
