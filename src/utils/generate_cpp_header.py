import sys
import itertools
from pathlib import Path
from functools import partial
from extract_data import extract_data

assert len(sys.argv) == 2
filepath = Path(sys.argv[1])
assert filepath.exists()

phases, components, contexts = extract_data(filepath)

dump = partial(print, end="")


def dump_enum_decription(enum_classname, entries):
    enum_name = enum_classname.lower()
    dump(
        f"""
inline std::string describe(const {enum_classname} {enum_name}, bool name_only=false) {{
  switch ({enum_name}) {{
"""
    )
    for name in entries:
        dump(
            f"""
    case {enum_classname}::{name}:
        if (name_only) return "{name}";
    return "{name} {enum_name}";
"""
        )
    dump(
        f"""
  }}
  assert(false);
  return "Unknown {enum_name}!";
}}

inline std::ostream &operator<<(std::ostream &os, const {enum_classname} {enum_name}) {{
    os << describe({enum_name});
    return os;
}}

#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
inline auto identify_{enum_name}(const {enum_classname} {enum_name}, bool name_only=false) {{
    return describe({enum_name}, name_only);
}}
#endif
"""
    )


dump(
    """#pragma once

/************************************************************

This file is generated from definitions at compile time and
                    is NOT to be edited.

*************************************************************/

#include <string>
#include <iostream>

#include <compass_cxx_utils/vsarray.h>

#include <physics/utilities.h>

namespace physics {
"""
)

# -- phases declaration --

dump(
    f"""
enum struct Phase : underlying_enum_type {{{ ", ".join(phases) }}};
using all_phases = Phase_set<{", ".join([f"Phase::{name}" for name in phases])}>;
constexpr auto nb_phases = all_phases::size;
"""
)

dump_enum_decription("Phase", phases)

# -- components declaration --

dump(
    f"""
enum struct Component : underlying_enum_type {{{ ", ".join(components) }}};
using all_components = Component_set<{", ".join([f"Component::{name}" for name in components])}>;
constexpr auto nb_components = all_components::size;
"""
)

dump_enum_decription("Component", components)

# -- contexts declaration --

dump(
    f"""
enum struct Context : underlying_enum_type {{{ ", ".join(contexts.keys()) }}};
using all_contexts = Context_set<{", ".join([f"Context::{name}" for name in contexts.keys()])}>;
constexpr auto nb_contexts = all_contexts::size;
"""
)

dump_enum_decription("Context", contexts.keys())

dump(
    """
using PresentPhases =
    Enum_tuple<all_contexts,"""
)
dump(
    ",".join(
        f"""
        // Context::{name}
        Phase_set<{", ".join(
            f"Phase::{s}" for s in data["present_phases"]
            )}>"""
        for name, data in contexts.items()
    )
)
dump(
    """
>;

template <Context context>
using Present_phases = PresentPhases::element_type<context>;

constexpr compass::utils::vsarray<Phase, nb_phases> present_phases_table[nb_contexts] {
"""
)
dump(
    ",\n".join(
        [
            "{" + ", ".join([f"Phase::{s}" for s in data["present_phases"]]) + "}"
            for data in contexts.values()
        ]
    )
)
dump(
    """
};

constexpr compass::utils::vsarray<Phase, nb_phases> absent_phases_table[nb_contexts] {
"""
)
dump(
    ",\n".join(
        [
            "{"
            + ", ".join(
                [f"Phase::{s}" for s in (set(phases) - set(data["present_phases"]))]
            )
            + "}"
            for data in contexts.values()
        ]
    )
)
dump(
    """
};

constexpr compass::utils::vsarray<Phase, nb_phases> union_present_phases_table[nb_contexts*nb_contexts] {
"""
)
dump(
    ",\n".join(
        [
            "{"
            + ", ".join(
                [
                    f"Phase::{s}"
                    for s in (
                        set(data1["present_phases"]) | set(data2["present_phases"])
                    )
                ]
            )
            + "}"
            for data1, data2 in itertools.product(contexts.values(), contexts.values())
        ]
    )
)
dump(
    """
};

constexpr compass::utils::vsarray<Phase, nb_phases> intersection_present_phases_table[nb_contexts*nb_contexts] {
"""
)
dump(
    ",\n".join(
        [
            "{"
            + ", ".join(
                [
                    f"Phase::{s}"
                    for s in (
                        set(data1["present_phases"]) & set(data2["present_phases"])
                    )
                ]
            )
            + "}"
            for data1, data2 in itertools.product(contexts.values(), contexts.values())
        ]
    )
)
dump(
    """
};

template <typename BinOp, typename PhaseFunctor>
  requires(std::is_same_v<all_phases, typename PhaseFunctor::definition_set>)
inline auto accumulate_on_present_phases(const Context context,
                                         const PhaseFunctor &F, const auto &x) {

  if constexpr (nb_phases == 1) {
    return std::get<0>(F.template elements)(x);
  } else {
    if constexpr (nb_contexts == 1) {
      return F.template accumulate<BinOp, all_phases>(x);
    } else {
      using result_type =
          std::invoke_result_t<decltype(F.template get<all_phases::first>()),
                               decltype(x)>;
      switch (context) {
"""
)
for name in contexts.keys():
    dump(
        f"""
      case Context::{name}:
        return F.template accumulate<BinOp, Present_phases<Context::{name}>>(x);
"""
    )
dump(
    """
      }
      assert(false);
      return result_type{};
    }
  }
}

} // namespace physics

#include <physics/definitions.h>
"""
)
