import numpy as np
from numba.types import double, Record, NestedArray


class States_with_contexts:
    def __init__(self, states, contexts):
        self.states = states
        self.contexts = contexts


class SaturationStruct:
    def __init__(self, n_phases):
        self.n_phases = n_phases
        self.St = Record.make_c_struct(
            [
                (
                    "saturation",
                    NestedArray(dtype=double, shape=(n_phases,)),
                ),
            ]
        )

    def empty_Xsat(self, len=None):
        if len is not None and len > 1:
            return np.empty(len, dtype=self.St).view(np.recarray)
        return np.empty((), dtype=self.St).view(np.recarray)

    def Xsat(self, ph_saturation):
        if np.ndim(ph_saturation) == 1:
            return np.rec.array((ph_saturation), dtype=self.St)
        elif np.ndim(ph_saturation) == 2:
            array_size = np.shape(ph_saturation)[0]
            X = self.empty_Xsat(array_size)
            X["saturation"] = ph_saturation
            return X


class TemperatureStruct:
    def __init__(self):
        self.Xt = Record.make_c_struct(
            [
                ("temperature", double),
            ]
        )

    def empty_Xtemp(self, len=None):
        if len is not None and len > 1:
            return np.empty(len, dtype=self.St).view(np.recarray)
        return np.empty((), dtype=self.St).view(np.recarray)

    def Xtemp(self, temperature):
        if np.ndim(temperature) == 0:
            return np.rec.array((temperature), dtype=self.St)
        elif np.ndim(temperature) == 1:
            array_size = np.size(temperature)
            X = self.empty_Xtemp(array_size)
            X["temperature"] = temperature
            return X


# one by phase, n_comp can differ ?
class PhaseStateStruct:
    def __init__(self, n_comp):
        self.n_comp = n_comp
        self.Xt = Record.make_c_struct(
            [
                ("pressure", double),
                ("temperature", double),
                (
                    "molar_fractions",
                    NestedArray(dtype=double, shape=(n_comp,)),
                ),
            ]
        )

    def Xalpha(self, pressure, temperature, molar_fractions=None):
        if molar_fractions is None:
            assert self.n_comp == 1, "Molar fractions missing in Xalpha"
            molar_fractions = np.ones(self.n_comp)
        if (
            np.isscalar(pressure)
            and np.isscalar(temperature)
            and np.ndim(molar_fractions) == 1
        ):
            return np.rec.array((pressure, temperature, molar_fractions), dtype=self.Xt)
        else:  # vector
            array_size = max(np.size(pressure), np.size(temperature))
            if np.ndim(molar_fractions) > 1:
                array_size = max(array_size, np.shape(molar_fractions)[0])
            X = self.empty_Xalpha(array_size)
            X["pressure"] = pressure
            X["temperature"] = temperature
            X["molar_fractions"] = molar_fractions
            return X

    def empty_Xalpha(self, len=None):
        if len is not None and len > 1:
            return np.empty(len, dtype=self.Xt).view(np.recarray)
        return np.empty((), dtype=self.Xt).view(np.recarray)


class Phases:
    def __init__(self, n_ph):
        assert n_ph == 2
        self.n_phases = n_ph
        self.gas = 0
        self.liquid = 1


class Components:
    def __init__(self, n_comp):
        assert n_comp == 2
        self.n_components = n_comp
        self.water = 0
        self.air = 1


class Physics:
    """
    Python Physics class which mimic the cpp class,
    should be used only to test custom physical properties !
    """

    def __init__(self, np, nc, name):
        self.phy_name = name
        self.Phase = Phases(np)
        self.Component = Components(nc)
        self.phase_state_type = PhaseStateStruct(nc)
        self.saturation_type = SaturationStruct(np)
        self.temperature_type = TemperatureStruct()
        self.nb_phases = np
        self.nb_components = nc

    def name(self):
        return self.phy_name

    def Xalpha(self, pressure, temperature, molar_fractions=None):
        return self.phase_state_type.Xalpha(
            pressure,
            temperature,
            molar_fractions,
        )

    def empty_Xalpha(self, len=None):
        return self.phase_state_type.empty_Xalpha(len)

    def empty_Xsat(self, len=None):
        return self.saturation_type.empty_Xsat(len)

    def Xsat(self, sat):
        return self.saturation_type.Xsat(sat)

    def empty_Xtemp(self, len=None):
        return self.temperature_type.empty_Xtemp(len)

    def Xtemp(self, temp):
        return self.temperature_type.Xtemp(temp)

    def build_molar_fractions(self, salt_molar_fraction):
        physics_name = self.name()
        if salt_molar_fraction is None:
            assert (
                physics_name == "linear_water"
            ), "Molar fractions missing in brine physics"
            molar_fractions = None
        else:
            assert (
                physics_name == "brine"
            ), "Do not give molar fractions in linear_water physics"
            # in brine physics, only Cs is given amoung the two components
            assert self.Component.salt == 0
            # transpose is necessary when salt_molar_fraction is an array
            molar_fractions = np.array(
                [salt_molar_fraction, 1.0 - salt_molar_fraction]
            ).T
        return molar_fractions
