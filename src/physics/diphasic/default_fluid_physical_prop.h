#pragma once
#include <compass_cxx_utils/array_utils.h>
#include <math.h>

// we allow array operations
using namespace compass::utils::array_operators;

namespace physics {

// molar enthalpy
Real gas_molar_enthalpy_with_derivatives(const Xalpha &X, Xalpha &dfdX) {
  Real M_air = 29.0e-3;
  Real M_H2O = 18.0e-3;
  Real a = 1990.89e3;
  Real b = 190.16e3;
  Real cc = -1.91264e3;
  Real d = 0.2997e3;
  Real CpGas = 1000.0;

  Real Ts = X.temperature / 100.0;
  Real ss = a + b * Ts + cc * std::pow(Ts, 2) + d * std::pow(Ts, 3);
  Real dssdT = (b + 2.0 * cc * Ts + 3.0 * d * std::pow(Ts, 2)) / 100.0;
  Real beta_air = CpGas * M_air;
  Real beta_water = M_H2O;
  Component_vector partial_molar_enthalpy;
  Component_vector dpartial_molar_enthalpydT;
  partial_molar_enthalpy[Component::water] = beta_water * ss;
  dpartial_molar_enthalpydT[Component::water] = beta_water * dssdT;
  partial_molar_enthalpy[Component::air] = beta_air * X.temperature;
  dpartial_molar_enthalpydT[Component::air] = beta_air;

  dfdX.pressure = 0;
  dfdX.molar_fractions = partial_molar_enthalpy;
  dfdX.temperature =
      std::inner_product(X.molar_fractions.begin(), X.molar_fractions.end(),
                         dpartial_molar_enthalpydT.begin(), 0.0);
  return std::inner_product(X.molar_fractions.begin(), X.molar_fractions.end(),
                            partial_molar_enthalpy.begin(), 0.0);
}

Real gas_molar_enthalpy_without_derivatives(const Xalpha &X) {
  Real M_air = 29.0e-3;
  Real M_H2O = 18.0e-3;
  Real a = 1990.89e3;
  Real b = 190.16e3;
  Real cc = -1.91264e3;
  Real d = 0.2997e3;
  Real CpGas = 1000.0;

  Real Ts = X.temperature / 100.0;
  Real ss = a + b * Ts + cc * std::pow(Ts, 2) + d * std::pow(Ts, 3);
  Real beta_air = CpGas * M_air;
  Real beta_water = M_H2O;
  Component_vector partial_molar_enthalpy;
  partial_molar_enthalpy[Component::water] = beta_water * ss;
  partial_molar_enthalpy[Component::air] = beta_air * X.temperature;
  return std::inner_product(X.molar_fractions.begin(), X.molar_fractions.end(),
                            partial_molar_enthalpy.begin(), 0.0);
}

Real liquid_molar_enthalpy_with_derivatives(const Xalpha &X, Xalpha &dfdX) {
  Real M_H2O = 18.0e-3;
  Real a = -14.4319e3;
  Real b = 4.70915e3;
  Real cc = -4.87534;
  Real d = 1.45008e-2;
  Real T0 = 273.0;

  Real TdegC = X.temperature - T0;
  Real ss = a + b * TdegC + cc * std::pow(TdegC, 2) + d * std::pow(TdegC, 3);
  Real dssdT = b + 2.0 * cc * TdegC + 3.0 * d * std::pow(TdegC, 2);
  // FIXME: all components have the same contributions

  dfdX.pressure = 0;
  std::fill(dfdX.molar_fractions.begin(), dfdX.molar_fractions.end(), 0.0);
  dfdX.temperature = dssdT * M_H2O;
  return M_H2O * ss;
}

Real liquid_molar_enthalpy_without_derivatives(const Xalpha &X) {
  Real M_H2O = 18.0e-3;
  Real a = -14.4319e3;
  Real b = 4.70915e3;
  Real cc = -4.87534;
  Real d = 1.45008e-2;
  Real T0 = 273.0;

  Real TdegC = X.temperature - T0;
  Real ss = a + b * TdegC + cc * std::pow(TdegC, 2) + d * std::pow(TdegC, 3);
  // FIXME: all components have the same contributions
  return M_H2O * ss;
}

auto default_molar_enthalpy_with_derivatives() {
  PhaseProperty<Phase_property_with_derivatives_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_molar_enthalpy_with_derivatives;
  phases_prop[Phase::liquid] = &liquid_molar_enthalpy_with_derivatives;
  return phases_prop;
}
auto default_molar_enthalpy_without_derivatives() {
  PhaseProperty<Phase_property_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_molar_enthalpy_without_derivatives;
  phases_prop[Phase::liquid] = &liquid_molar_enthalpy_without_derivatives;
  return phases_prop;
}

// viscosities
Real gas_viscosity_with_derivatives(const Xalpha &X, Xalpha &dfdX) {
  dfdX.pressure = 0;
  std::fill(dfdX.molar_fractions.begin(), dfdX.molar_fractions.end(), 0.0);
  dfdX.temperature = 0;
  return 2.0e-5;
}

Real gas_viscosity_without_derivatives(const Xalpha &X) { return 2.0e-5; }

Real liquid_viscosity_with_derivatives(const Xalpha &X, Xalpha &dfdX) {
  dfdX.pressure = 0;
  std::fill(dfdX.molar_fractions.begin(), dfdX.molar_fractions.end(), 0.0);
  dfdX.temperature = 0;
  return 1.0e-3;
}

Real liquid_viscosity_without_derivatives(const Xalpha &X) { return 1.0e-3; }

auto default_viscosity_with_derivatives() {
  PhaseProperty<Phase_property_with_derivatives_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_viscosity_with_derivatives;
  phases_prop[Phase::liquid] = &liquid_viscosity_with_derivatives;
  return phases_prop;
}
auto default_viscosity_without_derivatives() {
  PhaseProperty<Phase_property_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_viscosity_without_derivatives;
  phases_prop[Phase::liquid] = &liquid_viscosity_without_derivatives;
  return phases_prop;
}

// psat
// valid between -50C and 200C
Real psat_with_derivative(const Xtemp &X, Xtemp &dfdX) {
  Real Psat =
      100.0 * exp(46.784 - 6435.0 / X.temperature - 3.868 * log(X.temperature));
  dfdX.temperature =
      (6435.0 / std::pow(X.temperature, 2) - 3.868 / X.temperature) * Psat;
  return Psat;
}

// valid between -50C and 200C
Real psat_without_derivative(const Xtemp &X) {
  return 100.0 *
         exp(46.784 - 6435.0 / X.temperature - 3.868 * log(X.temperature));
}

auto default_psat_with_derivatives() { return &psat_with_derivative; }
auto default_psat_without_derivatives() { return &psat_without_derivative; }

// molar density
Real gas_molar_density_with_derivatives(const Xalpha &X, Xalpha &dfdX) {
  Real Rgp = 8.314;
  Real one_over_RgpT = 1.0 / (Rgp * X.temperature);
  dfdX.pressure = one_over_RgpT;
  dfdX.temperature = -X.pressure * one_over_RgpT / X.temperature;
  std::fill(dfdX.molar_fractions.begin(), dfdX.molar_fractions.end(), 0.0);
  return X.pressure * one_over_RgpT;
}

Real gas_molar_density_without_derivatives(const Xalpha &X) {
  Real Rgp = 8.314;
  return X.pressure / (Rgp * X.temperature);
}

// M_H2O = 18.0e-3
Real liquid_molar_density_with_derivatives(const Xalpha &X, Xalpha &dfdX) {
  dfdX.pressure = 0.0;
  std::fill(dfdX.molar_fractions.begin(), dfdX.molar_fractions.end(), 0.0);
  dfdX.temperature = 0.0;
  return 1000.0 / 18.0e-3;
}

Real liquid_molar_density_without_derivatives(const Xalpha &X) {
  return 1000.0 / 18.0e-3;
}

auto default_molar_density_with_derivatives() {
  PhaseProperty<Phase_property_with_derivatives_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_molar_density_with_derivatives;
  phases_prop[Phase::liquid] = &liquid_molar_density_with_derivatives;
  return phases_prop;
}
auto default_molar_density_without_derivatives() {
  PhaseProperty<Phase_property_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_molar_density_without_derivatives;
  phases_prop[Phase::liquid] = &liquid_molar_density_without_derivatives;
  return phases_prop;
}

// volumetric mass density
auto molar_to_mass_density_with_derivatives(const Xalpha &X, Xalpha &dfdX,
                                            Real phase_molar_density) {
  Component_vector comp_molar_mass;
  comp_molar_mass[Component::air] = 29.0e-3;
  comp_molar_mass[Component::water] = 18.0e-3;
  // Careful : I removed MCP from the formula
  // MCP(AIR_COMP, iph)*M_air*C(AIR_COMP) + MCP(WATER_COMP,
  // iph)*M_H2O*C(WATER_COMP)
  Real weighted_components_molar_mass =
      std::inner_product(X.molar_fractions.begin(), X.molar_fractions.end(),
                         comp_molar_mass.begin(), 0.0);
  dfdX.pressure *= weighted_components_molar_mass;
  dfdX.temperature *= weighted_components_molar_mass;
  auto weighted_phase_molar_density = comp_molar_mass;
  weighted_phase_molar_density *= phase_molar_density;
  dfdX.molar_fractions *= weighted_components_molar_mass;
  dfdX.molar_fractions += weighted_phase_molar_density;
  return weighted_components_molar_mass * phase_molar_density;
}

auto molar_to_mass_density_without_derivatives(const Xalpha &X,
                                               Real phase_molar_density) {
  Component_vector comp_molar_mass;
  comp_molar_mass[Component::air] = 29.0e-3;
  comp_molar_mass[Component::water] = 18.0e-3;
  // Careful : I removed MCP from the formula
  // I removed MCP from the formula
  // MCP(AIR_COMP, iph)*M_air*C(AIR_COMP) + MCP(WATER_COMP,
  // iph)*M_H2O*C(WATER_COMP)
  Real weighted_components_molar_mass =
      std::inner_product(X.molar_fractions.begin(), X.molar_fractions.end(),
                         comp_molar_mass.begin(), 0.0);
  return weighted_components_molar_mass * phase_molar_density;
}

Real gas_volumetric_mass_density_with_derivatives(const Xalpha &X,
                                                  Xalpha &dfdX) {
  Real phase_molar_density = gas_molar_density_with_derivatives(X, dfdX);
  return molar_to_mass_density_with_derivatives(X, dfdX, phase_molar_density);
}
Real gas_volumetric_mass_density_without_derivatives(const Xalpha &X) {
  Real phase_molar_density = gas_molar_density_without_derivatives(X);
  return molar_to_mass_density_without_derivatives(X, phase_molar_density);
}
Real liquid_volumetric_mass_density_with_derivatives(const Xalpha &X,
                                                     Xalpha &dfdX) {
  Real phase_molar_density = liquid_molar_density_with_derivatives(X, dfdX);
  return molar_to_mass_density_with_derivatives(X, dfdX, phase_molar_density);
}
Real liquid_volumetric_mass_density_without_derivatives(const Xalpha &X) {
  Real phase_molar_density = liquid_molar_density_without_derivatives(X);
  return molar_to_mass_density_without_derivatives(X, phase_molar_density);
}

auto default_volumetric_mass_density_with_derivatives(
    Component_vector &comp_molar_mass) {
  // careful, the value is duplicated, must coincide with value
  // in molar_to_mass_density_with_derivatives
  //  and in fonctions without derivatives
  comp_molar_mass[Component::air] = 29.0e-3;
  comp_molar_mass[Component::water] = 18.0e-3;

  PhaseProperty<Phase_property_with_derivatives_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_volumetric_mass_density_with_derivatives;
  phases_prop[Phase::liquid] = &liquid_volumetric_mass_density_with_derivatives;
  return phases_prop;
}
auto default_volumetric_mass_density_without_derivatives(
    Component_vector &comp_molar_mass) {
  // careful, the value is duplicated, must coincide with value
  // in molar_to_mass_density_without_derivatives
  //  and in fonctions with derivatives
  comp_molar_mass[Component::air] = 29.0e-3;
  comp_molar_mass[Component::water] = 18.0e-3;

  PhaseProperty<Phase_property_ptr> phases_prop;
  phases_prop[Phase::gas] = &gas_volumetric_mass_density_without_derivatives;
  phases_prop[Phase::liquid] =
      &liquid_volumetric_mass_density_without_derivatives;
  return phases_prop;
}

} // namespace physics
