#pragma once

namespace physics {

// relative permeabilities
Real gas_rel_perm_with_derivatives(const Xsat &X, Xsat &dfdX) {
  dfdX.saturation[Phase::liquid] = 0.0;
  dfdX.saturation[Phase::gas] = 2 * X.saturation[Phase::gas];
  return std::pow(X.saturation[Phase::gas], 2);
}

Real gas_rel_perm_without_derivatives(const Xsat &X) {
  return std::pow(X.saturation[Phase::gas], 2);
}

Real liquid_rel_perm_with_derivatives(const Xsat &X, Xsat &dfdX) {
  dfdX.saturation[Phase::gas] = 0.0;
  dfdX.saturation[Phase::liquid] = 2 * X.saturation[Phase::liquid];
  return std::pow(X.saturation[Phase::liquid], 2);
}

Real liquid_rel_perm_without_derivatives(const Xsat &X) {
  return std::pow(X.saturation[Phase::liquid], 2);
}

auto default_rel_perm_with_derivatives(auto &rocktypes) {
  rocktypes.push_back(0);
  RocktypePhaseProperty<Rock_property_with_derivatives_ptr> phases_prop(1);
  phases_prop[0][Phase::gas] = &gas_rel_perm_with_derivatives;
  phases_prop[0][Phase::liquid] = &liquid_rel_perm_with_derivatives;
  return phases_prop;
}
auto default_rel_perm_without_derivatives(auto &rocktypes) {
  rocktypes.push_back(0);
  RocktypePhaseProperty<Rock_property_ptr> phases_prop(1);
  phases_prop[0][Phase::gas] = &gas_rel_perm_without_derivatives;
  phases_prop[0][Phase::liquid] = &liquid_rel_perm_without_derivatives;
  return phases_prop;
}

// null capillary pressure
Real pref2pgas_with_derivatives(const Xsat &X, Xsat &dfdX) {
  std::fill(dfdX.saturation.begin(), dfdX.saturation.end(), 0.0);
  return 0.0;
}

Real pref2pgas_without_derivatives(const Xsat &X) { return 0.0; }

Real pref2pliquid_with_derivatives(const Xsat &X, Xsat &dfdX) {
  std::fill(dfdX.saturation.begin(), dfdX.saturation.end(), 0.0);
  return 0.0;
}

Real pref2pliquid_without_derivatives(const Xsat &X) { return 0.0; }

auto default_pref2palpha_with_derivatives(auto &rocktypes) {
  rocktypes.push_back(0);
  RocktypePhaseProperty<Rock_property_with_derivatives_ptr> phases_prop(1);
  phases_prop[0][Phase::gas] = &pref2pgas_with_derivatives;
  phases_prop[0][Phase::liquid] = &pref2pliquid_with_derivatives;
  return phases_prop;
}
auto default_pref2palpha_without_derivatives(auto &rocktypes) {
  rocktypes.push_back(0);
  RocktypePhaseProperty<Rock_property_ptr> phases_prop(1);
  phases_prop[0][Phase::gas] = &pref2pgas_without_derivatives;
  phases_prop[0][Phase::liquid] = &pref2pliquid_without_derivatives;
  return phases_prop;
}

// rock volumetric heat capacity by default 800.0*2000.0 J/m3
Real default_rock_volumetric_heat_capacity() { return 1.6e6; }

} // namespace physics
