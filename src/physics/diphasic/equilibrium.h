#pragma once

namespace physics {

inline void enforce_consistent_molar_fractions(Component_vector &mol_frac) {
  auto Ca = std::min(std::max(mol_frac[Component::air], 0.0), 1.0);
  mol_frac[Component::air] = Ca;
  mol_frac[Component::water] = 1.0 - Ca;
}

inline void
enforce_consistent_molar_fractions(Phase_component_matrix &mol_frac) {
  for (auto &&alpha : all_phases::array)
    enforce_consistent_molar_fractions(mol_frac[alpha]);
}

template <typename Fugacity_functor> // with derivatives
auto diphasic_equilibrium(const Fugacity_functor &fugacity,
                          const Phase_vector &pressure,
                          const double &temperature, const double atol = 1e-7,
                          std::size_t maxiter = 1000) {
  auto air_index = component_index(Component::air);
  auto water_index = component_index(Component::water);
  Xalpha Xgas{.pressure{pressure[Phase::gas]}, .temperature{temperature}};
  Xalpha Xliq{.pressure{pressure[Phase::liquid]}, .temperature{temperature}};
  // Newton start: no water in gas no air in liquid
  Xgas.molar_fractions[Component::air] = 1.0;
  Xliq.molar_fractions[Component::air] = 0.0;
  for (; maxiter != 0; --maxiter) {
    enforce_consistent_molar_fractions(Xgas.molar_fractions);
    enforce_consistent_molar_fractions(Xliq.molar_fractions);
    auto &&gas_fugacity = fugacity.apply(Phase::gas, Xgas);
    auto &&liquid_fugacity = fugacity.apply(Phase::liquid, Xliq);
    auto Ra = gas_fugacity.value[air_index] - liquid_fugacity.value[air_index];
    auto Rw =
        gas_fugacity.value[water_index] - liquid_fugacity.value[water_index];
    if (fabs(Ra) + fabs(Rw) < atol) {
      Phase_component_matrix mol_frac{};
      mol_frac[Phase::gas] = Xgas.molar_fractions;
      mol_frac[Phase::liquid] = Xliq.molar_fractions;
      return mol_frac;
    }
    // Newton: X -> X - J^-1 R
    // the unknown is the air molar fraction
    auto Jag = gas_fugacity.derivatives[air_index].molar_fractions[air_index] -
               gas_fugacity.derivatives[air_index].molar_fractions[water_index];
    auto Jwg =
        gas_fugacity.derivatives[water_index].molar_fractions[air_index] -
        gas_fugacity.derivatives[water_index].molar_fractions[water_index];
    auto Jal =
        -liquid_fugacity.derivatives[air_index].molar_fractions[air_index] +
        liquid_fugacity.derivatives[air_index].molar_fractions[water_index];
    auto Jwl =
        -liquid_fugacity.derivatives[water_index].molar_fractions[air_index] +
        liquid_fugacity.derivatives[water_index].molar_fractions[water_index];
    auto det = Jag * Jwl - Jwg * Jal;
    assert(det != 0);
    Xgas.molar_fractions[Component::air] -= (Jwl * Ra - Jal * Rw) / det;
    Xliq.molar_fractions[Component::air] -= (-Jwg * Ra + Jag * Rw) / det;
  }

  throw std::runtime_error(
      "Maximum number of iterations in diphasic_equilibrium exceeded.");
}

} // namespace physics
