#pragma once

#include <ranges>

namespace physics {

template <typename Fugacity_f, typename Fug_coef_f> struct Flash {
  const Fugacity_f fugacity;
  const Fug_coef_f fugacity_coefficients;

  Flash(const Fugacity_f &fug, const Fug_coef_f &fug_coef)
      : fugacity{fug}, fugacity_coefficients{fug_coef} {}

  template <typename State_field, typename Context_field>
  void update(State_field &states, Context_field &contexts) {

    for (auto &&[state, context] : std::views::zip(states, contexts)) {
      switch (context) {

      case Context::gas:
        gas_to_diphasic(state, context);
        break;
      case Context::liquid:
        liquid_to_diphasic(state, context);
        break;
      case Context::diphasic:
        diphasic_switches(state, context);
        break;
      }
      enforce_consistent_molar_fractions(state.molar_fractions);
    }
  }

  void gas_to_diphasic(State &state, Context &context) {
    auto air_index = component_index(Component::air);
    auto water_index = component_index(Component::water);
    auto &&fg = fugacity_coefficients.apply(Phase::gas, state).value;
    auto &&fl = fugacity_coefficients.apply(Phase::liquid, state).value;
    auto Cla = fg[air_index] / fl[air_index] *
               state.molar_fractions[Phase::gas][air_index];
    auto Clw = fg[water_index] / fl[water_index] *
               state.molar_fractions[Phase::gas][water_index];
    if (Cla + Clw > 1.0) { // liquid appears
      context = Context::diphasic;
      state.saturation[Phase::gas] = 1.0;
      state.saturation[Phase::liquid] = 0.0;
      state.molar_fractions[Phase::liquid][air_index] = Cla;
    }
  }

  void liquid_to_diphasic(State &state, Context &context) {
    auto air_index = component_index(Component::air);
    auto water_index = component_index(Component::water);
    auto &&fl = fugacity.apply(Phase::liquid, state).value;

    //   WARNING: the following relies on the ideal gas assumption
    //            and assumes fa = PaCag and fw = PgCwg
    //   WARNING: don't divide inequality by Pg (migth be negative during Newton
    //   iteration)
    if (fl[air_index] + fl[water_index] > state.pressure[Phase::gas]) {
      //  FIXME: is it ok to compare to Pg when gas is not ideal? (cf. issue
      //  #159)
      context = Context::diphasic;
      state.saturation[Phase::gas] = 0.0;
      state.saturation[Phase::liquid] = 1.0;
    }
  }

  void diphasic_switches(State &state, Context &context) {
    if (state.saturation[Phase::gas] < 0.0) { // gas vanishes
      context = Context::liquid;
      state.saturation[Phase::gas] = 0.0;
      state.saturation[Phase::liquid] = 1.0;
    } else if (state.saturation[Phase::liquid] < 0.0) { // liquid vanishes
      context = Context::gas;
      state.saturation[Phase::gas] = 1.0;
      state.saturation[Phase::liquid] = 0.0;
    }
  }
};

} // namespace physics
