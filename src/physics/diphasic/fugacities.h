#pragma once
#include <cmath>
#include <compass_cxx_utils/array_utils.h>
#include <ranges>

namespace physics {

// this file is temporary, will be defined in python
// this laws must be passed through python interface
// returns a function: component_vector = f(state, ddXs)

template <Phase ph, typename FluidProperties_with_d>
inline auto f_fugacity_coefficients(FluidProperties_with_d &fluid_properties) {
  using namespace compass::utils::array_operators;
  using std::views::zip;

  if constexpr (ph == Phase::gas) {
    return [](const Xalpha &X, std::array<Xalpha, nb_components> &ddXs) {
      Component_vector res;
      for (auto &&s : res)
        s = X.pressure;
      for (auto &&ddX : ddXs) {
        ddX = Xalpha{}; // init with 0
        ddX.pressure = 1.0;
      }
      return res;
    };
  } else if constexpr (ph == Phase::liquid) {
    auto &&psat_f = fluid_properties._psat;
    auto &&zeta_alpha_f = fluid_properties._molar_densities[ph];
    return [psat_f, zeta_alpha_f](const Xalpha &X,
                                  std::array<Xalpha, nb_components> &ddXs) {
      Component_vector res;

      for (auto &&comp : all_components::array) {
        auto icp = component_index(comp);

        if (comp == Component::air) { // Henry coef
          static const Real H1 = 6.e9;
          static const Real H2 = 10.e9;
          static const Real T1 = 293.e0;
          static const Real T2 = 353.e0;

          Real ddHT = (H2 - H1) / (T2 - T1);
          Real H = H1 + (X.temperature - T1) * ddHT; // Henry coef

          res[icp] = H;
          ddXs[icp] = Xalpha{}; // fill with 0
          ddXs[icp].temperature = ddHT;

        } else if (comp == Component::water) {
          static const Real R = 8.314;
          auto &&Xt = Xtemp{X.temperature};
          Xtemp ddXpsat;
          auto &&psat = psat_f(Xt, ddXpsat);
          Xalpha ddXzeta;
          auto &&zeta = zeta_alpha_f(X, ddXzeta);

          auto delta_p = X.pressure - psat;
          auto RTzeta = R * X.temperature * zeta;
          auto beta = delta_p / RTzeta;
          Xalpha ddXbeta;
          ddXbeta.pressure = (1. - delta_p * ddXzeta.pressure / zeta) / RTzeta;
          ddXbeta.temperature =
              (-ddXpsat.temperature -
               delta_p * (1. / X.temperature + ddXzeta.temperature / zeta)) /
              RTzeta;
          auto ebeta = exp(beta);
          res[icp] = psat * ebeta;
          ddXs[icp].pressure = ddXbeta.pressure * res[icp];
          ddXs[icp].temperature =
              (ddXpsat.temperature + ddXbeta.temperature * psat) * ebeta;
          for (auto &&[ddX, ddXzetaC] :
               zip(ddXs[icp].molar_fractions, ddXzeta.molar_fractions))
            ddX = -delta_p * ddXzetaC / zeta / RTzeta * res[icp];
        }
      }
      return res;
    };
  } else {
    throw std::runtime_error(
        "phase is not identified in f_fugacity_coefficients");
  }
}

// without derivatives
template <Phase ph, typename FluidProperties_without_d>
inline auto
f_fugacity_coefficients_no_d(FluidProperties_without_d &fluid_properties) {
  using namespace compass::utils::array_operators;

  if constexpr (ph == Phase::gas) {
    return [](const Xalpha &X) {
      Component_vector res;
      for (auto &&s : res)
        s = X.pressure;
      return res;
    };
  } else if constexpr (ph == Phase::liquid) {
    auto &&psat_f = fluid_properties._psat;
    auto &&zeta_alpha_f = fluid_properties._molar_densities[ph];
    return [psat_f, zeta_alpha_f](const Xalpha &X) {
      Component_vector res;

      for (auto &&comp : all_components::array) {
        auto icp = component_index(comp);

        if (comp == Component::air) { // Henry coef
          static const Real H1 = 6.e9;
          static const Real H2 = 10.e9;
          static const Real T1 = 293.e0;
          static const Real T2 = 353.e0;

          Real ddHT = (H2 - H1) / (T2 - T1);
          Real H = H1 + (X.temperature - T1) * ddHT; // Henry coef

          res[icp] = H;

        } else if (comp == Component::water) {
          static const Real R = 8.314;
          auto &&Xt = Xtemp{X.temperature};
          auto &&psat = psat_f(Xt);
          auto &&zeta = zeta_alpha_f(X);
          auto RTzeta = R * X.temperature * zeta;
          auto beta = (X.pressure - psat) / RTzeta;
          auto ebeta = exp(beta);
          res[icp] = psat * ebeta;
        }
      }
      return res;
    };
  } else {
    throw std::runtime_error(
        "phase is not identified in f_fugacity_coefficients_no_d");
  }
}
} // namespace physics
