#pragma once
#include <array>

namespace physics {

struct Prim_sec_indices {
  // tmp
  // create only unk_indices with a generator and use accessors to other values
  // build vectors without knowing the order of contexts !

  static constexpr Context_vector<std::array<int, nb_dof>> prim_unk_indices = {
      // gas context, prim unkowns : Pref(1), T(3), Cag(4)
      std::array<int, nb_dof>{0, 2, 4}, // gas
      // diphasic context, prim unkowns : Pref(1), T(3), Sg(7)
      std::array<int, nb_dof>{0, 2, 7}, // diphasic
      // liquid context, prim unkowns : Pref(1), T(3), Cal(6)
      std::array<int, nb_dof>{0, 2, 6} // liquid
  };
  static constexpr Context_vector<std::array<int, nb_all_clos_laws>>
      sec_unk_indices = {
          // sec unkowns : tmp : all others variables
          std::array<int, nb_all_clos_laws>{1, 3, 5, 6, 7, 8}, // gas
          std::array<int, nb_all_clos_laws>{1, 3, 4, 5, 6, 8}, // diphasic
          std::array<int, nb_all_clos_laws>{1, 3, 4, 5, 7, 8}  // liquid
  };
  static constexpr Context_vector<std::array<int, nb_natural_variables>>
      unk_indices = {
          // prim sec columns permutation
          std::array<int, nb_natural_variables>{0, 2, 4, 1, 3, 5, 6, 7,
                                                8}, // gas
          std::array<int, nb_natural_variables>{0, 2, 7, 1, 3, 4, 5, 6,
                                                8}, // diphasic
          std::array<int, nb_natural_variables>{0, 2, 6, 1, 3, 4, 5, 7,
                                                8} // liquid
  };
  static constexpr Context_vector<std::array<int, nb_natural_variables>>
      var_indices = {
          // variables order from unknowns order
          std::array<int, nb_natural_variables>{0, 3, 1, 4, 2, 5, 6, 7,
                                                8}, // gas
          std::array<int, nb_natural_variables>{0, 3, 1, 4, 5, 6, 7, 2,
                                                8}, // diphasic
          std::array<int, nb_natural_variables>{0, 3, 1, 4, 5, 6, 2, 7,
                                                8} // liquid
  };
};

struct Align {
  // tmp
  // create with a generator !
  // build vectors without knowing the order of contexts and components !

  // The first eq should be continuity equation : sum of Molar mass * molar
  // balance would make disapear the molecular diffusion if the state is at
  // equilibrium, only Darcy remains !

  static constexpr Context_vector<int[nb_dof * nb_dof]> mat = {
      // gas context
      1, 1, 0, // Pref :continuity eq (sum palpha)
      0, 0, 1, // T: energy conservation
      0, 1, 0, // Cag: air conservation
               // diphasic context
      1, 1, 0, // Pref :continuity eq (sum palpha)
      0, 0, 1, // T: energy conservation
      1, 0, 0, // Sg: water conservation
               // liquid context
      1, 1, 0, // Pref :continuity eq (sum palpha)
      0, 0, 1, // T: energy conservation
      1, 0, 0  // Cal: water conservation
  };
};

} // namespace physics
