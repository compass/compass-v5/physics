import numpy as np


def accumulation_dict(acc, components_dict, prefix=""):
    acc_dict = {}
    for comp, i in components_dict.items():
        acc_dict[f"{prefix}{comp} eq"] = acc.as_array().molar[:, i]
    acc_dict[f"{prefix}thermal eq"] = acc.as_array().energy
    return acc_dict


def states_dict(states, contexts, phases_dict, components_dict, prefix=""):
    state_dict = {}
    state_dict[f"{prefix}context"] = contexts.as_array()
    state_dict[f"{prefix}temperature"] = states.as_array().temperature
    for phase, alpha in phases_dict.items():
        state_dict[f"{prefix}{phase} pressure"] = states.as_array().pressure[:, alpha]
        state_dict[f"{prefix}{phase} saturation"] = states.as_array().saturation[
            :, alpha
        ]
        mf = states.as_array().molar_fractions
        for comp, i in components_dict.items():
            state_dict[f"{prefix}{comp} molar fraction in {phase}"] = mf[:, alpha, i]
    return state_dict


def dump_states(filename, states, contexts, phases_dict, components_dict):
    np.savez(filename, **states_dict(states, contexts, phases_dict, components_dict))
