import verstr

# FIXME: to import nanobind field_base object defined in icus
import icus

try:
    from . import _version

    __version__ = verstr.verstr(_version.version)
except ImportError:
    __version__ = None


def load(name):
    import importlib
    import compass

    with compass.nanobind():
        return importlib.import_module(f".{name}", package="physics")


def phases_dict(module):
    Phase = module.Phase
    return {alpha: repr(Phase(alpha)) for alpha in range(module.nb_phases)}


def components_dict(module):
    Component = module.Component
    return {i: repr(Component(i)) for i in range(module.nb_components)}


def contexts_dict(module):
    Context = module.Context
    return {context: repr(Context(context)) for context in range(module.nb_contexts)}
