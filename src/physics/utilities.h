#pragma once

#include <cstdint>

#include <compass_cxx_utils/enum_functors.h>
#include <compass_cxx_utils/enum_set.h>
#include <compass_cxx_utils/enum_tuple.h>

namespace physics {

using namespace compass::utils;

using underlying_enum_type = char;
const char *underlying_enum_dtype = "c";

enum struct Phase : underlying_enum_type;
template <Phase... phase> using Phase_set = Enum_set<Phase, phase...>;

enum struct Context : underlying_enum_type;
template <Context... context> using Context_set = Enum_set<Context, context...>;

enum struct Component : underlying_enum_type;
template <Component... component>
using Component_set = Enum_set<Component, component...>;

} // namespace physics
