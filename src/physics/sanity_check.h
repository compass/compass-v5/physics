#pragma once

#include <icus/types.h>
#include <physics/physical_state.h>

namespace physics {

inline void test_init_state(const State_field &f, const icus::index_type &i) {
  auto x = f(i);
  Real eps = 1.e-7;
  assert(abs(x.temperature - 300) < eps);
  assert(abs(x.saturation[0] - 0.4) < eps);
  assert(abs(x.molar_fractions[0][0] - 0.1) < eps);
  assert(abs(x.molar_fractions[1][0] - 0.8) < eps);
  Real sat_sum = 0.;
  for (size_t alpha = 0; alpha < nb_phases; ++alpha) {
    sat_sum += x.saturation[alpha];
    assert(abs(x.pressure[alpha] - 1.e5) < eps);
    Real comp_sum = 0.;
    for (size_t comp = 0; comp < nb_components; ++comp) {
      comp_sum += x.molar_fractions[alpha][comp];
    }
    assert(abs(1.0 - comp_sum) < eps);
  }
  assert(abs(1.0 - sat_sum) < eps);
}

} // namespace physics
