#pragma once

// this file is to be included after model definition

namespace physics {

template <typename... Fs>
  requires(sizeof...(Fs) == nb_phases)
inline auto phase_functors(Fs &&...fs) {
  return Enum_functors<all_phases, std::decay_t<Fs>...>{
      std::forward_as_tuple(std::forward<Fs>(fs)...)};
}

template <typename... Fs>
  requires(sizeof...(Fs) == nb_contexts)
inline auto context_functors(Fs &&...fs) {
  return Enum_functors<all_contexts, std::decay_t<Fs>...>{
      std::forward_as_tuple(std::forward<Fs>(fs)...)};
}

template <typename... Fs>
  requires(sizeof...(Fs) == nb_components)
inline auto component_functors(Fs &&...fs) {
  return Enum_functors<all_components, std::decay_t<Fs>...>{
      std::forward_as_tuple(std::forward<Fs>(fs)...)};
}

template <typename PhaseFunctor>
inline auto sum_on_present_phases(const Context context, const PhaseFunctor &F,
                                  const auto &x) {
  return accumulate_on_present_phases<binops::add>(context, F, x);
}

inline constexpr auto phase_index = compass::utils::index<all_phases>;
inline constexpr auto component_index = compass::utils::index<all_components>;
inline constexpr auto context_index = compass::utils::index<all_contexts>;

constexpr inline auto present_phases(const Context context) {
  return present_phases_table[context_index(context)];
}

constexpr inline auto absent_phases(const Context context) {
  return absent_phases_table[context_index(context)];
}

inline auto union_present_phases(const Context c1, const Context c2) {
  return union_present_phases_table[context_index(c1) * nb_contexts +
                                    context_index(c2)];
}

inline auto intersection_present_phases(const Context c1, const Context c2) {
  return intersection_present_phases_table[context_index(c1) * nb_contexts +
                                           context_index(c2)];
}

} // namespace physics
