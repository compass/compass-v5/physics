#pragma once

#include <array>
#include <vector>

namespace physics {

static constexpr std::size_t nb_natural_variables =
    2 * nb_phases + 1 + nb_components * nb_phases; // size of(State)
//    // FIXME: preprocessor directives to be removed!
// #ifdef _THERMIQUE_
static constexpr std::size_t nb_dof = nb_components + 1;
// #else
//    static constexpr std::size_t nbdof = nc;
// #endif
static constexpr std::size_t nb_all_clos_laws = nb_natural_variables - nb_dof;

struct Context_info {
  std::size_t nb_equilibrium{};
  std::vector<Component> component;         // size is nb_equilibrium
  std::vector<std::array<Phase, 2>> phases; // size is nb_equilibrium
  std::size_t nb_secondary_unknowns;
};

inline auto init_contexts_info() {
  std::array<Context_info, nb_contexts> contexts_info;
  // for (auto &&context : all_contexts::array) {
  //     std::size_t nb_eq = 0; // comptor
  //     for (auto &&alpha : present_phases(context)) {
  //     }
  // }
  //     for (auto &&comp : all_components::array) {

  // }
  // enum struct Context : std::size_t {gas, diphasic, liquid};
  if constexpr (nb_contexts > 1) {                   // avoid pure
    contexts_info[1].nb_equilibrium = nb_components; // diphasic
    for (auto &&comp : all_components::array) {
      contexts_info[1].component.push_back(comp);
      assert(nb_phases == 2 && "draft, only for diphasic");
      std::array<Phase, 2> phase_pair;
      for (auto &&alpha : all_phases::array) {
        phase_pair[phase_index(alpha)] = alpha;
      }
      contexts_info[1].phases.push_back(phase_pair);
    }
  }

  // for (auto &&context : all_contexts::array) {
  // nb_secondary_unknowns is equal to the number of closure laws :
  // thermodynamic equilibrium + sum(sat)
  // + sum(c_i^\alpha) for all \alpha in present_phases
  //     contexts_info[context_index(context)].nb_secondary_unknowns =
  //         nb_equilibrium + 1 + present_phases(context);
  // }
  if constexpr (nb_contexts > 1) {           // avoid pure
    contexts_info[0].nb_secondary_unknowns = // gas
        contexts_info[0].nb_equilibrium + 1 + 1;
    contexts_info[1].nb_secondary_unknowns = // diphasic
        contexts_info[1].nb_equilibrium + 1 + 2;
    contexts_info[2].nb_secondary_unknowns = // liquid
        contexts_info[2].nb_equilibrium + 1 + 1;
  }

  return contexts_info;
}

} // namespace physics
