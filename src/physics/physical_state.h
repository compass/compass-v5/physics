#pragma once

#include <array>
#include <compass_cxx_utils/array_utils.h>
#include <cstddef>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <physics/physical_info.h>

namespace physics {

using Real = double;
// we allow array operations
using namespace compass::utils::array_operators;

struct Phase_vector : std::array<Real, nb_phases> {
  using base = std::array<Real, nb_phases>;
  using base::operator[];
  using base::operator=;
  auto &operator[](const Phase alpha) {
    return this->base::operator[](phase_index(alpha));
  }
  const auto &operator[](const Phase alpha) const {
    return this->base::operator[](phase_index(alpha));
  }
};

struct Component_vector : std::array<Real, nb_components> {
  using base = std::array<Real, nb_components>;
  using base::operator[];
  using base::operator=;
  auto &operator[](const Component comp) {
    return this->base::operator[](component_index(comp));
  }
  const auto &operator[](const Component comp) const {
    return this->base::operator[](component_index(comp));
  }
};

struct Phase_component_matrix : std::array<Component_vector, nb_phases> {
  using base = std::array<Component_vector, nb_phases>;
  using base::operator[];
  using base::operator=;
  auto &operator[](const Phase alpha) {
    return this->base::operator[](phase_index(alpha));
  }
  const auto &operator[](const Phase alpha) const {
    return this->base::operator[](phase_index(alpha));
  }
};

template <typename T> struct Context_vector : std::array<T, nb_contexts> {
  using base = std::array<T, nb_contexts>;
  using base::operator[];
  using base::operator=;
  auto &operator[](const Context ctx) {
    return this->base::operator[](context_index(ctx));
  }
  const auto &operator[](const Context ctx) const {
    return this->base::operator[](context_index(ctx));
  }
};

struct Xalpha {
  using value_t = Real;
  Real pressure = 0;
  Real temperature = 0;
  Component_vector molar_fractions = {0};
};

struct Xsat {
  using value_t = Real;
  Phase_vector saturation = {0};
};

struct Xtemp {
  using value_t = Real;
  Real temperature = 0;
};

struct State {
  using value_t = Real;
  Phase_vector pressure = {0};
  Real temperature = 0;
  Phase_component_matrix molar_fractions = {0};
  Phase_vector saturation = {0};
};

using State_field = icus::Field<State>;
using Scalar_field = icus::Field<Real>;
using Context_field = icus::Field<Context>;
using Rocktype_field = icus::Labels;

inline auto get_Xalpha(Phase alpha, const State &x) {
  return Xalpha{.pressure = x.pressure[alpha],
                .temperature = x.temperature,
                .molar_fractions = x.molar_fractions[alpha]};
}

template <Phase alpha> struct State2Xalpha {
  using From = State;
  using To = Xalpha;
  To v;
  State2Xalpha(const From &x)
      : v{.pressure = x.pressure[alpha],
          .temperature = x.temperature,
          .molar_fractions = x.molar_fractions[alpha]} {}
  From d(const To &ddXalpha) {
    From res{};
    res.pressure[alpha] = ddXalpha.pressure;
    res.molar_fractions[alpha] = ddXalpha.molar_fractions;
    res.temperature = ddXalpha.temperature;
    return res;
  }
};

struct State2Xsat {
  using From = State;
  using To = Xsat;
  To v;
  State2Xsat(const From &x) : v{.saturation = x.saturation} {}
  From d(const To &ddXsat) {
    From res{};
    res.saturation = ddXsat.saturation;
    return res;
  }
};

struct State2Xtemp {
  using From = State;
  using To = Xtemp;
  To v;
  State2Xtemp(const From &x) : v{.temperature = x.temperature} {}
  From d(const To &ddXtemp) {
    From res{};
    res.temperature = ddXtemp.temperature;
    return res;
  }
};

struct Accumulation {
  Component_vector molar = {0};
  Real energy = 0;

  template <typename T> inline Accumulation &operator*=(const T &lambda) {
    this->molar *= lambda;
    this->energy *= lambda;
    return *this;
  }
  template <typename T> inline Accumulation &operator/=(const T &lambda) {
    this->molar /= lambda;
    this->energy /= lambda;
    return *this;
  }
  inline Accumulation &operator+=(const Accumulation &other) {
    this->molar += other.molar;
    this->energy += other.energy;
    return *this;
  }
  template <typename T> inline Accumulation operator*(const T &lambda) {
    Accumulation res = *this;
    res *= lambda;
    return res;
  }
};

using Acc_field = icus::Field<Accumulation>;

using Phase_property_with_derivatives_ptr = Real (*)(const Xalpha &, Xalpha &);
using Phase_property_ptr = Real (*)(const Xalpha &);
using Rock_property_with_derivatives_ptr = Real (*)(const Xsat &, Xsat &);
using Rock_property_ptr = Real (*)(const Xsat &);
using Temp_property_with_derivatives_ptr = Real (*)(const Xtemp &, Xtemp &);
using Temp_property_ptr = Real (*)(const Xtemp &);

template <typename prop_ptr>
struct PhaseProperty : std::array<prop_ptr, nb_phases> {
  using base = std::array<prop_ptr, nb_phases>;
  using base::operator[];
  auto &operator[](const Phase alpha) {
    return this->base::operator[](phase_index(alpha));
  }
  const auto &operator[](const Phase alpha) const {
    return this->base::operator[](phase_index(alpha));
  }
};

template <typename prop_ptr>
using RocktypePhaseProperty = std::vector<PhaseProperty<prop_ptr>>;

} // namespace physics
