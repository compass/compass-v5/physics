#pragma once

#include "diphasic/model.h"
#include <physics/physical_info.h>
#include <physics/physical_state.h>

#include "diphasic/default_fluid_physical_prop.h"
#include "diphasic/default_rock_physical_prop.h"
#include "diphasic/equilibrium.h"
#include "diphasic/flash.h"
#include "diphasic/fugacities.h"
#include "diphasic/primary_secondary_alignement.h"
