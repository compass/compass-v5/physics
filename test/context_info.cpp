#include <physics/model.h>
#include <physics/physical_info.h>

#include <cassert>
#include <iostream>

int main() {
  using namespace physics;

  auto &&context_info = init_contexts_info();
  assert(context_info[context_index(Context::gas)].nb_equilibrium == 0);
  assert(context_info[context_index(Context::liquid)].nb_equilibrium == 0);
  for (auto &&context : all_contexts::array) {

    auto &&ctx_info = context_info[context_index(context)];

    auto nb_eq = ctx_info.nb_equilibrium;
    std::cout << "for " << identify_context(context) << ", nb equilibrium is ";
    std::cout << nb_eq << std::endl;
    for (auto eq = 0; eq < nb_eq; ++eq) {
      std::cout << "  - for the " << identify_component(ctx_info.component[eq]);
      auto &&[alpha, beta] = ctx_info.phases[eq];
      std::cout << ", phases are " << identify_phase(alpha);
      std::cout << " and " << identify_phase(beta) << std::endl;
    }
  }

  return 0;
}
