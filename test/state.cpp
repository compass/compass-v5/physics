#include <physics/model.h>
#include <physics/physical_state.h>

#include <cassert>
#include <iostream>

int main() {
  using namespace physics;

  auto pg = 10.0;
  auto pl = 15.0;
  auto temp = 25.0;
  auto X0 = State{{pg, pl}, temp, {0, 1, 1, 0}, {1, 0}};

  assert(abs(X0.temperature - temp) < 1e-7);

  // test zero initialisation
  auto all_zero = [](const auto &a) {
    for (auto &&x : a) {
      if (x != 0) {
        std::cerr << "non zero values:";
        for (auto &&y : a) {
          std::cerr << " " << y;
        }
        std::cerr << std::endl;
        return false;
      }
    }
    return true;
  };

  State S;
  assert(S.pressure.size() == nb_phases);
  assert(all_zero(S.pressure));
  assert(S.temperature == 0);
  assert(S.molar_fractions.size() == nb_phases);
  for (auto &&Ca : S.molar_fractions) {
    assert(all_zero(Ca));
  }
  assert(S.saturation.size() == nb_phases);
  assert(all_zero(S.saturation));

  Accumulation A;

  assert(A.molar.size() == nb_components);
  assert(all_zero(A.molar));
  assert(A.energy == 0);

  return 0;
}
