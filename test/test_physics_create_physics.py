def test_create_physics():
    from physics.physical_state import Physics

    nb_components = 2
    nb_phases = 2
    # creates python physics object (for example to test
    # the python physical properties outside of compass)
    diphasic_physics = Physics(nb_phases, nb_components, "diphasic")
