#include <iostream>

#include <physics/model.h>

int main() {

  using namespace physics;

  std::cout << "Size of phase enum in bytes: " << sizeof(Phase::gas)
            << std::endl;

  // compile time context
  std::cout << "Phases present in diphasic context:";
  for (auto &&alpha : Present_phases<Context::diphasic>::array)
    std::cout << " " << identify_phase(alpha) << " (size of enum in bytes "
              << sizeof(alpha) << "),";
  std::cout << std::endl;

  // runtime context
  Context context = Context::diphasic;
  std::cout << "Phases present in " << identify_context(context) << ":";
  for (auto &&alpha : present_phases(context))
    std::cout << " " << identify_phase(alpha) << ",";
  std::cout << std::endl;

  std::cout << "Size of present phase buffer: "
            << sizeof(present_phases(context)) << " bytes" << std::endl;

  // union of phases present in two contexts
  for (auto &&c1 : all_contexts::array) {
    for (auto &&c2 : all_contexts::array) {
      std::cout << "Phases present in union of " << identify_context(c1)
                << " and " << identify_context(c2) << ":";
      for (auto &&alpha : union_present_phases(c1, c2))
        std::cout << " " << identify_phase(alpha) << ",";
      std::cout << std::endl;
    }
  }

  // intersection of phases present in two contexts
  for (auto &&c1 : all_contexts::array) {
    for (auto &&c2 : all_contexts::array) {
      const auto intersection = intersection_present_phases(c1, c2);
      std::cout << (std::size_t)intersection.size()
                << " phases present in intersection of " << identify_context(c1)
                << " and " << identify_context(c2) << ":";
      for (auto &&alpha : intersection)
        std::cout << " " << identify_phase(alpha) << ",";
      std::cout << std::endl;
    }
  }
}
