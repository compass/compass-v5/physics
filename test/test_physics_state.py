from icus import ISet, field
import physics


def test_state():

    the_physics = physics.load("diphasic")
    assert the_physics.nb_components == 2
    assert the_physics.nb_phases == 2
    sites = ISet(10)
    Xcoats = field(sites, dtype=the_physics.State)
    xcoats = Xcoats[0]
    xcoats.temperature = 300.0
    xcoats.saturation[the_physics.Phase.gas] = 0.4
    xcoats.saturation[the_physics.Phase.liquid] = (
        1.0 - xcoats.saturation[the_physics.Phase.gas]
    )
    xcoats.molar_fractions[the_physics.Phase.gas][the_physics.Component.water] = 0.1
    xcoats.molar_fractions[the_physics.Phase.liquid][the_physics.Component.water] = 0.8
    for alpha in range(the_physics.nb_phases):
        xcoats.molar_fractions[alpha][the_physics.Component.air] = (
            1.0 - xcoats.molar_fractions[alpha][the_physics.Component.water]
        )
        xcoats.pressure[alpha] = 1.0e5
    the_physics.test_init_state(Xcoats, 0)
    assert physics.phases_dict(the_physics)[the_physics.Phase.gas] == "gas"
    assert physics.phases_dict(the_physics)[the_physics.Phase.liquid] == "liquid"
    assert physics.components_dict(the_physics)[the_physics.Component.water] == "water"
    assert physics.components_dict(the_physics)[the_physics.Component.air] == "air"
    assert physics.contexts_dict(the_physics)[the_physics.Context.gas] == "gas"
    assert physics.contexts_dict(the_physics)[the_physics.Context.liquid] == "liquid"
    assert (
        physics.contexts_dict(the_physics)[the_physics.Context.diphasic] == "diphasic"
    )


def set_accumulation():
    the_physics = physics.load("diphasic")
    I = ISet(6)
    Acc = field(I, dtype=the_physics.Accumulation)
    assert len(Acc.molar) == the_physics.nb_components
    Acc.energy = 20.6
