import numpy as np
from icus import ISet, field
import physics


def test_iset():
    diph_physics = physics.load("diphasic")

    I = ISet(10)
    porosity = field(I)
    porosity[...] = 0.15
    assert np.allclose(porosity.as_array(), 0.15)

    assert diph_physics.nb_components == 2
    assert diph_physics.nb_phases == 2

    # fill a field directly
    state = field(I, dtype=diph_physics.State)
    state_ar = state.as_array()
    state_ar.temperature[:] = 300.0
    Sg = state_ar.saturation[:, diph_physics.Phase.gas]
    Sg = 0.4
    state_ar.saturation[:, diph_physics.Phase.liquid] = 1.0 - Sg
    state_ar.molar_fractions[
        :, diph_physics.Phase.gas, diph_physics.Component.water
    ] = 0.1
    state_ar.molar_fractions[
        :, diph_physics.Phase.liquid, diph_physics.Component.water
    ] = 0.8
    for alpha in range(diph_physics.nb_phases):
        state_ar.molar_fractions[:, alpha, diph_physics.Component.air] = (
            1.0 - state_ar.molar_fractions[:, alpha, diph_physics.Component.water]
        )
        state_ar.pressure[:, alpha] = 1.0e5

    # create a state
    S = diph_physics.State()
    S.temperature = 300
    # ...

    # or just retrieve a value from another field
    S = state[0]

    coats_state = field(I, dtype=diph_physics.State)
    coats_state[...] = S
    c_state_ar = coats_state.as_array()
    assert np.allclose(c_state_ar.temperature, 300.0)
    assert np.allclose(c_state_ar.pressure[:, diph_physics.Phase.gas], 1.0e5)
    assert np.allclose(c_state_ar.pressure[:, diph_physics.Phase.liquid], 1.0e5)
    assert np.allclose(
        c_state_ar.molar_fractions[
            :, diph_physics.Phase.gas, diph_physics.Component.water
        ],
        0.1,
    )

    contexts = field(I, dtype=diph_physics.Context)
    J = I.extract(list(range(0, I.size(), 2)))
    contexts[...] = diph_physics.Context.liquid
    assert np.all(contexts == diph_physics.Context.liquid)
    contexts[J] = diph_physics.Context.gas
    assert np.all((contexts == diph_physics.Context.gas)[::2])
    assert np.all((contexts == diph_physics.Context.liquid)[1::2])
