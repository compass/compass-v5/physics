
#include <physics/model.h>

using namespace physics;

// a function that is built from a phase value
struct A_property {
  Phase phase;
  double parameter;
  auto operator()(const auto &x) {
    return x; // done
  }
};

// a factory that use a phase template argument
template <Phase phase, typename F>
auto make_variable(F &&f) -> std::decay_t<F> {
  return f; // nothing done
}

// First strategy ----------------------------------------------------
// a factory that builds a quantity (such as accumulation)
// that is a phase functor
// parameters can be passes as factory arguments
template <Phase... ph>
auto make_quantity(Phase_set<ph...>,
                   const std::array<double, nb_phases> &parameters) {
  return phase_functors(
      make_variable<ph>(A_property{ph, parameters[phase_index(ph)]})...);
}

// Second strategy ----------------------------------------------------
// encapsulate everything in a class

template <typename PhaseSet> struct Phase_quantity;

template <Phase... ph> struct Phase_quantity<Phase_set<ph...>> {
  using phase_set = Phase_set<ph...>;
  // you can set any type or arguments through tje construtor
  std::array<double, sizeof...(ph)> params;
  // the receipe to build the functor for phase alpha
  template <Phase alpha> auto functor() const {
    return make_variable<alpha>(
        A_property{alpha, params[index<phase_set>(alpha)]});
  }
  auto all_functors() const { return phase_functors(functor<ph>()...); }
};

int main() {

  // could come from python
  std::array<double, nb_phases> parameters{1, 2};

  // First strategy ----------------------------------------------------
  auto quantity = make_quantity(all_phases{}, parameters);

  // Second strategy ----------------------------------------------------
  // instantiate the factory
  auto factory = Phase_quantity<all_phases>{parameters};
  // then build the phase functor (this can be delayed)
  auto the_same_quantity_as_above = factory.all_functors();
}
