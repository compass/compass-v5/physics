
#include <physics/model.h>

struct Rho {
  double param; // a functor parameter
  auto operator()(const double &x) const { return param * x; }
};

int main() {

  using namespace physics;

  using gas_phase = Phase_set<Phase::gas>;
  using liquid_phase = Phase_set<Phase::liquid>;
  using two_phases = Phase_set<Phase::gas, Phase::liquid>;
  static_assert(std::is_same_v<all_phases, two_phases>);

  auto rhog = Rho{.param = 1};
  auto rhol = Rho{.param = 2};

  auto F = phase_functors(rhog, rhol);
  // compile-time
  auto x = F.sum<gas_phase>(1);
  x = F.sum<two_phases>(1);
  x = F.get<Phase::gas>()(1);
  // runtime (implies a lookup)
  x = F.apply(Phase::gas, 1);
  const Phase gas = Phase::gas;
  x = F.apply(gas, 1);

  // sum on present phases at compile time
  x = F.sum<Present_phases<Context::diphasic>>(1);
  // sum on present phases at runtime
  Context context = Context::diphasic;
  x = sum_on_present_phases(context, F, 1);

  static_assert(std::is_same_v<all_phases, two_phases>);
  static_assert(std::is_same_v<Present_phases<Context::diphasic>, all_phases>);

  assert(F.sum<gas_phase>(1) == 1);
  assert(F.sum<liquid_phase>(1) == 2);
  assert(F.sum<two_phases>(1) == 3);
  assert(F.sum<Present_phases<Context::diphasic>>(1) == 3);
  assert(sum_on_present_phases(context, F, 1) == 3);
}
