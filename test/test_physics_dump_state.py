import numpy as np
from icus import ISet, field
import physics
from physics.dump import dump_states


def test_dump():
    diph_physics = physics.load("diphasic")
    I = ISet(10)
    Xcoats = field(I, dtype=diph_physics.State)
    for xcoats in Xcoats.as_array():
        xcoats.temperature = 300.0
        xcoats.saturation[diph_physics.Phase.gas] = 0.4
        xcoats.saturation[diph_physics.Phase.liquid] = (
            1.0 - xcoats.saturation[diph_physics.Phase.gas]
        )
        xcoats.molar_fractions[diph_physics.Phase.gas][
            diph_physics.Component.water
        ] = 0.1
        xcoats.molar_fractions[diph_physics.Phase.liquid][
            diph_physics.Component.water
        ] = 0.8
        for alpha in range(diph_physics.nb_phases):
            xcoats.molar_fractions[alpha][diph_physics.Component.air] = (
                1.0 - xcoats.molar_fractions[alpha][diph_physics.Component.water]
            )
            xcoats.pressure[alpha] = 1.0e5

    contexts = field(I, dtype=diph_physics.Context)
    for i in range(int((I.size() + 1) / 2)):
        contexts[2 * i] = diph_physics.Context.gas
        if i < I.size():
            contexts[2 * i + 1] = diph_physics.Context.liquid

    # inverse key : value to give phases and components dict
    ph_dict = {
        phase: alpha for alpha, phase in physics.phases_dict(diph_physics).items()
    }
    comp_dict = {comp: i for i, comp in physics.components_dict(diph_physics).items()}
    dump_states("dumptest", Xcoats, contexts, ph_dict, comp_dict)
    loaded = np.load("dumptest.npz")
    print("Xcoats loaded")
    for k, v in loaded.items():
        print(k, v)
    loaded.close()


if __name__ == "__main__":
    test_dump()
