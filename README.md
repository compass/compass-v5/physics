# How to integrate a new physics

- Define physics elements in `src/include/physics/*physics-name*/model.yml`.
- Add a `src/include/physics/*physics-name*.h` file that will include all necessary headers to your physics starting with `*physics-name*/model.h` (that will be generated from `src/include/physics/*physics-name*/model.yml`).
- List all headers in the file `src/include/${name}-physics-headers` starting with the generated file `physics/*physics-name*/model.h`.

As an example flash routines could be in a `src/include/physics/*physics-name*/flash.h` file and should be included via `src/include/physics/*physics-name*.h` (cF. the diphasic physics example).
